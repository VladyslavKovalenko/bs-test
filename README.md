# bs-test

This repository was created for debugging purposes only. Feel free to submit MRs :) 

## Prerequisites

The `allure` and `mvn` CLI should be installed.


## How to perform local test run.

1) Go to the root directory
2) Execute the following command
```bash
$ mvn clean test
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for org.example:bs-test:jar:1.0-SNAPSHOT
[WARNING] 'dependencies.dependency.(groupId:artifactId:type:classifier)' must be unique: org.testng:testng:jar -> duplicate declaration of version ${testng.version} @ line 34, column 21
[WARNING] 'dependencies.dependency.(groupId:artifactId:type:classifier)' must be unique: io.appium:java-client:jar -> duplicate declaration of version ${appium.version} @ line 57, column 21
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] ------------------------< org.example:bs-test >-------------------------
[INFO] Building bs-test 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
...
...
...
```

## Get allure report

1) Generate allure report, using the following command
```bash
allure generate target/allure-results -o allure-report
```
2) Open HTML allure report
```bash
allure open
```