package aequus.android;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class BeforeMethodDebugTest {

    // public AppiumDriver<MobileElement> driver;

    @BeforeTest
    @Parameters( { "deviceIndex" })
    public void preCondition(String deviceIndex, final ITestContext testContext) {
        // 1) parse get config from the path src/test/resources/aequus.android/bs.conf.json
        // 2) collect all capabilities from the path  (based on deviceIndex), including:
        /*
            capabilities.setCapability("project", getAndroidApkVersionFromConfig());
            capabilities.setCapability("build", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(OffsetDateTime.now(ZoneOffset.UTC)));
            capabilities.setCapability("name", testContext.getName());
            capabilities.setCapability("browserstack.local", true);
            capabilities.setCapability("browserstack.networkLogs", false);
         */
        // 3) Init driver with collected capabilities (driver = new AppiumDriver<MobileElement>(url, caps);)
        System.out.println("In Before Test\n\n" +
                "Expected string: (firstTest | secondTest)\n" +
                "Actual string: " + testContext.getName());
    }

    @Test
    public void firstTest() {
        // test steps
        // test assertions
        System.out.println("\n\nIn firstTest\n\n");
    }

    @Test
    public void secondTest() {
        // test steps
        // test assertions
        System.out.println("\n\nIn secondTest\n\n");
    }

    @AfterMethod
    public void postCondition(final ITestResult testResult) {
        // driver.quit();
        System.out.println("\n\nIn After Method\n\n" +
                "Expected string: (firstTest | secondTest)\n" +
                "Actual string: " + testResult.getName());
    }
}
